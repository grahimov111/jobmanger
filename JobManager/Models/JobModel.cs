﻿using JobManager.Constant;
using System;

namespace JobManager.Models
{
    public class JobModel
    {
        public string JobID { get; set; }

        public string Name { get; set; }
        public int? Floor { get; set; }
        public string JobStatus { get; set; }
        public string RoomType { get; set; }

        public string StatusColor
        {
            get
            {
                if (string.IsNullOrWhiteSpace(JobStatus))
                    return "table-light";


                if (string.Equals(JobStatus, ConstantText.StatusInProgress, StringComparison.InvariantCultureIgnoreCase))
                {
                    return "table-primary";
                }
                else if (string.Equals(JobStatus, ConstantText.StatusNotStarted, StringComparison.InvariantCultureIgnoreCase))
                {
                    return "table-light";
                }
                else if (string.Equals(JobStatus, ConstantText.StatusCompleted, StringComparison.InvariantCultureIgnoreCase))
                {
                    return "table-success";
                }
                else if (string.Equals(JobStatus, ConstantText.StatusDelayed, StringComparison.InvariantCultureIgnoreCase))
                {
                    return "table-danger";
                }

                return "table-light";
            }
        }

        public bool IsMarkable
        {
            get
            {
                if (string.IsNullOrWhiteSpace(JobStatus))
                    return false;

                if (string.Equals(JobStatus, ConstantText.StatusInProgress, StringComparison.InvariantCultureIgnoreCase) ||
                    string.Equals(JobStatus, ConstantText.StatusDelayed, StringComparison.InvariantCultureIgnoreCase))
                    return true;

                return false;
            }
        }
    }
}
