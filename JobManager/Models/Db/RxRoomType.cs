﻿using System;
using System.Collections.Generic;

namespace JobManager.Models.Db
{
    public partial class RxRoomType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
