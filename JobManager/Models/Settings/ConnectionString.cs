﻿using Newtonsoft.Json;

namespace JobManager.Models.Settings
{
    public class ConnectionString
    {
        [JsonProperty("defaultConnectionString")]
        public string DefaultConnectionString { get; set; }
    }
}
