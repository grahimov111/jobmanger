﻿using System;
using System.Collections.Generic;
using System.Linq;
using JobManager.DataBaseContext;
using JobManager.Models.Settings;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace JobManager.Controllers.API
{
    [Route("v0/api/JobSummary")]
    [ApiController]
    public class JobSummaryController : ControllerBase
    {
        private readonly ILogger<JobSummaryController> _logger;
        private readonly ConnectionString _connectionString;

        public JobSummaryController(IOptions<ConnectionString> options, ILogger<JobSummaryController> logger)
        {
            _logger = logger;
            _connectionString = options.Value;
        }

        [HttpGet("getJobStatusSummary", Name = nameof(GetJobStatusSummary))]
        public IActionResult GetJobStatusSummary()
        {
            var result = new List<object>();

            try
            {
                
                using (var context = new DemoContext(_connectionString.DefaultConnectionString))
                {
                    var roomTypes = context.RxRoomType.ToList();

                    foreach (var status in context.RxJob.ToList().GroupBy(_ => _.Status))
                    {
                        foreach (var subGroup in status.ToList().GroupBy(_ => _.RoomTypeId))
                        {
                            result.Add(new
                            {
                                Status = status.Key,
                                RoomType = roomTypes.FirstOrDefault(_=>_.Id ==  subGroup.Key)?.Name,
                                StatusCount = subGroup.Count()
                            });
                        }
                    }
                }

                return Ok(result);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"Error while trying to crete room status summary - {ex.Message}");

                return StatusCode(500, $"Error while trying to crete room status summary");
            }
        }
    }
}