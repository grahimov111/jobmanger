﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using JobManager.Models;
using JobManager.DataBaseContext;
using JobManager.Constant;
using Microsoft.Extensions.Options;
using JobManager.Models.Settings;

namespace JobManager.Controllers
{
    public class JobController : Controller
    {
        private readonly ILogger<JobController> _logger;
        private readonly ConnectionString _connectionString;
        public JobController(IOptions<ConnectionString> options, ILogger<JobController> logger)
        {
            _logger = logger;
            _connectionString = options.Value;
        }

        public IActionResult Index()
        {
            try
            {
                using (var context = new DemoContext(_connectionString.DefaultConnectionString))
                {
                    var model =
                        from j in context.RxJob
                        join r in context.RxRoomType on j.RoomTypeId equals r.Id
                        select new JobModel()
                        {
                            JobID = j.Id.ToString(),
                            Name = j.Name,
                            Floor = j.Floor,
                            JobStatus = j.Status,
                            RoomType = r.Name
                        };

                    return View(model.ToList());
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"{nameof(MarkCompleted)} - Error while trying to fetch Job data from database.");

                return View(new List<JobModel>());
            }
        }

        public IActionResult MarkCompleted(string jobId)
        {
            try
            {
                if(string.IsNullOrEmpty(jobId) || !Guid.TryParse(jobId, out Guid guid))
                    return new JsonResult(new { success = false, error = ConstantText.ErrorInvalidJobId });

                using (var context = new DemoContext(_connectionString.DefaultConnectionString))
                {
                  var toBeUpdated = context.RxJob.FirstOrDefault(_ => _.Id == guid);

                    if (toBeUpdated != null)
                    {
                        toBeUpdated.Status = ConstantText.StatusCompleted;
                        context.SaveChanges();
                    }
                    else
                    {
                        return new JsonResult(new { success = false, error = ConstantText.ErrorNoJobWasFound });
                    }
                }

                return new JsonResult(new { success = true, id = jobId});
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, $"{nameof(MarkCompleted)} - Error while trying to Update job status.");

                return new JsonResult(new { success = false, error = ex.Message});
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
