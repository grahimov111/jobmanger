﻿namespace JobManager.Constant
{
    public static class ConstantText
    {
        public static string ErrorInvalidJobId = "Invalid Job Id was provided.";
        public static string ErrorNoJobWasFound = "No job was found with a given Id.";
        
        public static string StatusCompleted = "Complete";
        public static string StatusInProgress = "In Progress";
        public static string StatusNotStarted = "Not Started";
        public static string StatusDelayed = "Delayed";
    }
}
